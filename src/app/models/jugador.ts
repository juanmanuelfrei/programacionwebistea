import { Equipo } from './equipo';
export interface Jugador {
    id        : number,
    nombre    : string,
    apellido  : string,
    dorsal    : number,
    cantGoles : number,
    equipo?   : Equipo,
    fecha     : string,
}
