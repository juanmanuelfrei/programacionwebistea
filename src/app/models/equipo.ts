import { Jugador } from "./jugador";

export interface Equipo {
    id?           : number,
    nombre        : string,
    posicion      : number,
    puntos        : number,
    fechaCreacion : string,
    activo        : boolean,
    jugadores?    : Jugador[],
}