import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login/login.component.css'] // CSS del login component
})
export class RegisterComponent implements OnInit {

  public registroForm: FormGroup;

  constructor(private _fb: FormBuilder, private _router: Router) { }

  get UsuarioNoValido() { return this.registroForm.get('usuario').invalid && this.registroForm.get('usuario').touched; }
  get PasswordNoValido() { return this.registroForm.get('password').invalid && this.registroForm.get('password').touched; }
  get Password2NoValido() { return this.registroForm.get('password2').invalid && this.registroForm.get('password2').touched; }

  ngOnInit(): void {
    this.cargarFormulario();
  }

  cargarFormulario() {
    this.registroForm = this._fb.group({
      usuario : ['', [Validators.required, Validators.email] ],
      password: ['', Validators.required ],
      password2: ['', Validators.required ],
      recordar: [false]
    } , {
      validators: this.passwordsIguales('password', 'password2')
    })
  }

  registrar() {
    this.registroForm.invalid ? this.formularioInvalido() : this.goLogin();
  }

  private formularioInvalido(){
    return Object.values(this.registroForm.controls).forEach(control => {
      control instanceof FormGroup ? Object.values(control.controls).forEach(() => control.markAllAsTouched()) : control.markAllAsTouched();
    });
  }

  private goLogin() {
    this.registroForm.get('recordar').value ? localStorage.setItem('usuario', this.registroForm.get('usuario').value) : null ;
    Swal.fire('Usuario registrado',`Ya podes ingresar con el usuario - ${this.registroForm.get('usuario').value}`, 'success');
    this._router.navigateByUrl('/login');
  }


  passwordsIguales(pass1: string, pass2: string) {
    return ( formGroup: FormGroup ) => {
      const pass1Control = formGroup.get(pass1);
      const pass2Control = formGroup.get(pass2);

      if (pass1Control.value === pass2Control.value) {
        pass2Control.setErrors(null);
      } else {
        pass2Control.setErrors({noEsIgual: true});
      }

    }
  }

}
