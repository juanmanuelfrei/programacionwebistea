import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(private _login: LoginService, private _fb: FormBuilder, private _router: Router) { }

  ngOnInit(): void {
    this.cargarFormulario();
    this.verificarRecordar();
  }

  get UsuarioNoValido() { return this.loginForm.get('usuario').invalid && this.loginForm.get('usuario').touched; }
  get PasswordNoValido() { return this.loginForm.get('password').invalid && this.loginForm.get('password').touched; }

  cargarFormulario() {
    this.loginForm = this._fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required],
      recordar: [false]
    })
  }

  login() {
    this.loginForm.invalid ? this.formularioInvalido() : this.ingresar();
  }

  private formularioInvalido() {
    return Object.values(this.loginForm.controls).forEach(control => {
      control instanceof FormGroup ? Object.values(control.controls).forEach(() => control.markAllAsTouched()) : control.markAllAsTouched();
    });
  }

  private ingresar() {
    const recordar = this.loginForm.get('recordar').value;
    const userLoged = this.loginForm.get('usuario').value;
    recordar ? localStorage.setItem('usuario', userLoged) : localStorage.removeItem('usuario');
    this._login.generarToken();
    this._router.navigateByUrl('/home');
  }

  verificarRecordar() {
    const usuarioAlmacenado = localStorage.getItem('usuario');
    if (usuarioAlmacenado) {
      this.loginForm.get('usuario').setValue(usuarioAlmacenado);
      this.loginForm.get('recordar').setValue(true);
    }
  }

}
