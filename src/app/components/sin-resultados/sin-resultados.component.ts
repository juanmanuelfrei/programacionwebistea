import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sin-resultados',
  templateUrl: './sin-resultados.component.html',
  styleUrls: ['./sin-resultados.component.css']
})
export class SinResultadosComponent  {

  @Input() mensaje: string = 'No hay resultados';

  constructor() { }

}
