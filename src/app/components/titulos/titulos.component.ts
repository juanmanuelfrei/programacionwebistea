import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Equipo } from '../../models/equipo';

@Component({
  selector: 'app-titulos',
  templateUrl: './titulos.component.html',
  styleUrls: ['./titulos.component.css']
})
export class TitulosComponent implements OnInit {

  @Input() titulo: string = '';
  @Input() nombreEquipo: string = '';

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.cargarDatosParametro();
  }

  private cargarDatosParametro() {
    this._activatedRoute.params.subscribe(
      resp => {
        if(resp.id > 0 ) this.titulo = 'Modificar jugador';
      }
    )
  }

}
