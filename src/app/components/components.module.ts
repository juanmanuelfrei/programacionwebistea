import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoadingComponent } from './loading/loading.component';
import { TitulosComponent } from './titulos/titulos.component';
import { SinResultadosComponent } from './sin-resultados/sin-resultados.component';

@NgModule({
  declarations: [
    LoadingComponent,
    TitulosComponent,
    SinResultadosComponent,
  ],
  exports:[
    LoadingComponent,
    TitulosComponent,
    SinResultadosComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
