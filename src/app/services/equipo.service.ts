import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Equipo } from '../models/equipo';

const api = 'api/Equipos'; // por temas de proxy

@Injectable({
  providedIn: 'root'
})
export class EquipoService {
  
  constructor(private http: HttpClient) { }  

  getEquipos() {
    return this.http.get(api);
  }

  getEquipoById(id: number) {
    return this.http.get(`${api}/${id}`);
  }

  borrarEquipo(id: number) {
    return this.http.delete(`${api}/${id}`);
  }

  crearEquipo(equipo) {
    return this.http.post(`${api}/altaequipo`, equipo );
  }

}
