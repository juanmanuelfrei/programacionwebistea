import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  generarToken() {
    return localStorage.setItem('token', '1234');
  }

  /*                               ************************                                 */

  estaAutenticado(): boolean {
    const token = localStorage.getItem('token') || '';
    return token.length > 0;
  }
}
