import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const api = 'api/Jugadores'; //por temas de proxy

@Injectable({
  providedIn: 'root'
})
export class JugadorService  {

  constructor(private http: HttpClient) { }

  getJugadores() {
    return this.http.get(api);
  }

  getJugadorById(id: number) {
    return this.http.get(`${api}/${id}`);
  }

  borrarJugador(id: number) {
    return  this.http.delete(`${api}/${id}`);
  }

  modificarGoles(id:number, accion: 'sumar' | 'restar') {
    console.log({id, accion});
    return this.http.post(`${api}/actualizajugador/gol`,  {id, accion}  );
  }
  

  // crearJugador(jugador): Jugador[] {
  //   jugador.id = this.jugadores.length+1;
  //   jugador.cantGoles = 0;
  //   this.jugadores.push(jugador);
  //   return this.jugadores;
  // }


  // modificarJugador(idJugdaor: number, jugadorModificado: Jugador) {
  //   let jugadorViejo = this.buscarJugadorPorId(idJugdaor);
  //   jugadorViejo.equipo = jugadorModificado.equipo;
  //   jugadorViejo.dorsal = jugadorModificado.dorsal;
  //   jugadorViejo.nombre = jugadorModificado.nombre;
  //   jugadorViejo.apellido = jugadorModificado.apellido;
  //   jugadorViejo.fecha = jugadorModificado.fecha;
  // }

}
