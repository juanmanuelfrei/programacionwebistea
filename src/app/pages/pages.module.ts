import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

//Modulos
import { SharedModule } from '../shared/shared.module';

//Componentes
import { PagesComponent } from './pages.component';
import { EquiposComponent } from './equipos/equipos.component';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { JugadorComponent } from './jugadores/jugador/jugador.component';
import { HomeComponent } from './home/home.component';
import { ComponentsModule } from '../components/components.module';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { EquipoComponent } from './equipos/equipo/equipo.component';
import { HttpClientModule } from '@angular/common/http';
import { ListadoJugadoresComponent } from './equipos/listado-jugadores/listado-jugadores.component';

@NgModule({
  declarations: [
    EquiposComponent,
    JugadoresComponent,
    JugadorComponent,
    HomeComponent,
    PagesComponent,
    BusquedaComponent,
    EquipoComponent,
    ListadoJugadoresComponent,
  ],
  exports: [
    HomeComponent,
    PagesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ComponentsModule,
    HttpClientModule
  ]
})
export class PagesModule { }
