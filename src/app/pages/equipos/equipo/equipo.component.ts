import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { EquipoService } from '../../../services/equipo.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-equipo',
  templateUrl: './equipo.component.html',
  styleUrls: ['./equipo.component.css']
})
export class EquipoComponent implements OnInit {

  public equipoForm: FormGroup

  constructor(private _formBuilder: FormBuilder, private _equipoService: EquipoService, private router: Router) { }

  get NombreNoValido()   { return this.equipoForm.get('nombre').invalid && this.equipoForm.get('nombre').touched; }
  get FechaNoValido()    { return this.equipoForm.get('fechaCreacion').invalid && this.equipoForm.get('fechaCreacion').touched; }

  ngOnInit(): void {
    this.crearFormulario();
  }

  crearFormulario() {
    this.equipoForm = this._formBuilder.group({
      nombre: ['', [Validators.required,Validators.minLength(2)] ],
      fechaCreacion : ['', [Validators.required] ],
      posicion: 1,
      puntos: 0,
      activo: true,
      jugadores: [],
    })
  }

  //Metodo submit del formulario
  cargarDatos() {
    this.equipoForm.valid ? this.crearEquipo() : this.formularioInvalido();
  }

  private formularioInvalido(){
    return Object.values(this.equipoForm.controls).forEach(control => {
      control instanceof FormGroup ? Object.values(control.controls).forEach(() => control.markAllAsTouched()) : control.markAllAsTouched();
    });
  }

  private crearEquipo() {
    console.log(this.equipoForm.value);
    this._equipoService.crearEquipo(this.equipoForm.value).subscribe(
      resp => {
        Swal.fire('Creado', 'Diste de alta un equipo!', 'success');
        this.router.navigateByUrl('/home/equipos');
      },
      err => {
        console.warn('errores');
      }
    )
  }

  limpiarForm() {
    this.equipoForm.reset();
  }

}
