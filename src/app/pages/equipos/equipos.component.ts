import { Component, OnInit } from '@angular/core';


//Servicios
import { EquipoService } from '../../services/equipo.service';
import { JugadorService } from '../../services/jugador.service';

//Modelos
import { Equipo } from 'src/app/models/equipo';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-equipos',
  templateUrl: './equipos.component.html',
  styleUrls: ['./equipos.component.css']
})
export class EquiposComponent implements OnInit {
  
  public listaEquipos: Equipo[] = [];
  public loading: boolean = true;

  constructor(private _equipoService: EquipoService, private _jugadorService: JugadorService) { }

  ngOnInit(): void {
    this.getEquipos();
  }
  
  getEquipos(){
    this._equipoService.getEquipos().subscribe(
      (resp: Equipo[]) => {
        this.loading = false;
        this.listaEquipos = resp;
        console.log(this.listaEquipos);
      },
      err => console.warn('no se pudo listar los equipos')
    )
  }

  borrarEquipo(id: number) {
    Swal.fire({
      title: '¿Quiere borrar al equipo?',
      text: "El cambio es irreversible",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this._equipoService.borrarEquipo(id).subscribe(
          resp => this.getEquipos(),
          err  => console.warn('no se pudo listar los equipos')
        )
        Swal.fire('Eliminado!','El equipo fue eliminado con exito.','success')
      }
    })
  }

}
