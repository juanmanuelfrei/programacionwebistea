import { Component, OnInit } from '@angular/core';
import { Equipo } from '../../../models/equipo';

@Component({
  selector: 'app-listado-jugadores',
  templateUrl: './listado-jugadores.component.html',
  styleUrls: ['./listado-jugadores.component.css']
})
export class ListadoJugadoresComponent implements OnInit {

  public team: Equipo;
  public hayEquipo: boolean = false;
  
  constructor() { }

  ngOnInit(): void {
  }

}
