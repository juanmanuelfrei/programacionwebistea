import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { JugadorService } from '../../services/jugador.service';
import { EquipoService } from 'src/app/services/equipo.service';

import { Jugador } from '../../models/jugador';
import { Equipo } from 'src/app/models/equipo';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  public jugadoresBuscados: Jugador[] = [];
  public equiposBuscados: Equipo[] = [];
  public loading: boolean = true;

  constructor(private _jugadorService: JugadorService, private _equipoService: EquipoService, private _activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(
      resp => {
        let intervalo = setInterval(() => {
          this.loading = false
          // this.equiposBuscados = this.cargarEquipos(resp.text);
          // this.jugadoresBuscados = this.cargarJugadores(resp.text);
          clearInterval(intervalo);
        },1500)
      }
    )
  }

  // cargarJugadores(busqueda: string): Jugador[] {
  //   return this._jugadorService.jugadores.filter(jugador => jugador.nombre.includes(busqueda) || jugador.apellido.includes(busqueda) );
  // }

  // cargarEquipos(busqueda: string): Equipo[] {
  //   return this._equipoService.equipos.filter(equipo => equipo.nombre.includes(busqueda));
  // }

}
