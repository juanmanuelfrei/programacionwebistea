import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { JugadorComponent } from './jugadores/jugador/jugador.component';
import { JugadoresComponent } from './jugadores/jugadores.component';
import { EquiposComponent } from './equipos/equipos.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { EquipoComponent } from './equipos/equipo/equipo.component';
import { LoginGuard } from '../guards/login.guard';
import { ListadoJugadoresComponent } from './equipos/listado-jugadores/listado-jugadores.component';

const routes: Routes = [
    { 
        path: 'home', component: PagesComponent,
        canActivate: [ LoginGuard ],
        canLoad: [ LoginGuard ],
        children: [
            { path: '', component: HomeComponent },

            //Jugadores
            { path: 'jugadores', component: JugadoresComponent },
            { path: 'jugador/:id', component: JugadorComponent },

            //Equipos
            { path: 'equipos', component: EquiposComponent },
            { path: 'equipo', component: EquipoComponent },
            { path: 'equipo/lista-jugadores/:equipo', component: ListadoJugadoresComponent },

            //Busqueda
            { path: 'buscar/:text', component: BusquedaComponent },
        ],
    },
];    

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {}
