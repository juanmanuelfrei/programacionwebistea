import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

import { Jugador } from '../../models/jugador';

import { JugadorService } from '../../services/jugador.service';

@Component({
  selector: 'app-jugadores',
  templateUrl: './jugadores.component.html',
  styleUrls: ['./jugadores.component.css']
})
export class JugadoresComponent implements OnInit {

  public listaJugadores: Jugador[] = [];
  public loading: boolean = true;

  constructor(private _jugadorService: JugadorService) { }

  ngOnInit(): void {
    this.cargarListaJugadores();
  }
  
  private cargarListaJugadores(){
    this._jugadorService.getJugadores().subscribe(
      (resp: Jugador[]) => {
        this.loading = false;
        this.listaJugadores = resp;
      },
      err => console.warn('No se pudieron cargar los jugadores.')
    )
  }
  
  borrarJugador(id: number){
    Swal.fire({
      title: '¿Quiere borrar al jugador?',
      text: "El cambio es irreversible",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this._jugadorService.borrarJugador(id).subscribe(
          resp => this.cargarListaJugadores(),
          err  => console.log(err)
        );
        Swal.fire('Eliminado!','El jugador fue eliminado con exito.','success');
      }
    })
  }

  modificarGoles(jugador: Jugador, accion: 'sumar' | 'restar') {
    this._jugadorService.modificarGoles(jugador.id, accion).subscribe(
      resp => console.log(resp),
      err  => console.log(err)
    )
  }

}
