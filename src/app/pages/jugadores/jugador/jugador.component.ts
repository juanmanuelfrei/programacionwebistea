import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';

import { JugadorService } from '../../../services/jugador.service';
import { EquipoService } from '../../../services/equipo.service';

import { Equipo } from 'src/app/models/equipo';


@Component({
  selector: 'app-jugador',
  templateUrl: './jugador.component.html',
  styleUrls: ['./jugador.component.css']
})
export class JugadorComponent implements OnInit {

  public equiposDisponibles: Equipo[] = [];
  public jugadorForm: FormGroup;
  public tituloBoton: string = 'Crear';
  public idJugadorModificar: number;

  constructor(private _formBuilder: FormBuilder, private _jugadorService: JugadorService, private _activatedRoute: ActivatedRoute, private router: Router, private _equipoService: EquipoService) { }

  ngOnInit(): void {
    this.crearFormulario();
    this.validarDatosParam();
    this.listarEquiposDisponibles();
  }

  get NombreNoValido() { return this.jugadorForm.get('nombre').invalid && this.jugadorForm.get('nombre').touched; }
  get ApellidoNoValido() { return this.jugadorForm.get('apellido').invalid && this.jugadorForm.get('apellido').touched; }
  get DorsalNoValido() { return this.jugadorForm.get('dorsal').invalid && this.jugadorForm.get('dorsal').touched; }
  get EquipoNoValido() { return this.jugadorForm.get('equipo').invalid && this.jugadorForm.get('equipo').touched; }
  get FechaNoValido() { return this.jugadorForm.get('fecha').invalid && this.jugadorForm.get('fecha').touched; }


  private crearFormulario() {
    this.jugadorForm = this._formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      apellido: ['', [Validators.required, Validators.minLength(2)]],
      dorsal: ['', Validators.required],
      equipo: ['', Validators.required],
      fecha: ['', Validators.required]
    })
  }

  private validarDatosParam() {
    this._activatedRoute.params.subscribe(
      resp => {
        this.idJugadorModificar = parseInt(resp.id);
        if (this.idJugadorModificar > 0) {
          this.tituloBoton = 'Modificar';
          this.cargarJugadorDeParametro(this.idJugadorModificar);
        }
      }
    )
  }

  private listarEquiposDisponibles() {
    this._equipoService.getEquipos().subscribe(
      (resp: Equipo[]) => {
        this.equiposDisponibles = resp;
      },
      err => console.warn('No se pudieron traer los equipos')
    )
  }

  private cargarJugadorDeParametro(id: number) {
    let jugadorBuscado;
    this._jugadorService.getJugadorById(id).subscribe(
      (resp: any) => {
        jugadorBuscado = resp;
        this.jugadorForm.patchValue(jugadorBuscado);
      }
    )
  }

  //Metodo submit del formulario
  cargarDatos() {
    if (this.jugadorForm.invalid) {
      this.formularioInvalido();
    } else {
      // this.tituloBoton == 'Crear' ? this.crearJugador() : this.modificarJugador();
    }
  }

  private formularioInvalido() {
    return Object.values(this.jugadorForm.controls).forEach(control => {
      control instanceof FormGroup ? Object.values(control.controls).forEach(() => control.markAllAsTouched()) : control.markAllAsTouched();
    });
  }

  // private crearJugador() {
  //   this.setEquipo();
  //   this._jugadorService.crearJugador(this.jugadorForm.value);
  //   Swal.fire('Jugador creado', 'Diste de alta un jugador!', 'success');
  //   this.router.navigateByUrl('/home/jugadores');
  // }

  // private modificarJugador() {
  //   this.setEquipo();
  //   this._jugadorService.modificarJugador(this.idJugadorModificar, this.jugadorForm.value);
  //   Swal.fire('Jugador modificado', 'Modificaste un jugador!', 'success');
  //   this.router.navigateByUrl('/home/jugadores');
  // }

  limpiarForm() {
    this.jugadorForm.reset();
  }

  // setEquipo() {
  //   let cambio;
  //   let equipoAModificar = this.jugadorForm.get('equipo').value;
  //   if (typeof (equipoAModificar) === 'string') {
  //     cambio = this._equipoService.equipos.find(equipo => equipo.nombre === equipoAModificar.toLowerCase());
  //     this.jugadorForm.patchValue({ equipo: cambio })
  //   }
  // }

}
