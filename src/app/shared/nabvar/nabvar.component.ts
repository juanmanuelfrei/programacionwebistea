import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nabvar',
  templateUrl: './nabvar.component.html',
  styleUrls: ['./nabvar.component.css']
})
export class NabvarComponent  {

  constructor(private router: Router) { }

  buscar(termino: string) {
    if (termino.length === 0) return;
    this.router.navigateByUrl(`/home/buscar/${termino}`);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigateByUrl('/login');
  }

}
