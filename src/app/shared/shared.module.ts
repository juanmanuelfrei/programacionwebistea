import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Modulos
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

//Componentes
import { FooterComponent } from './footer/footer.component';
import { NabvarComponent } from './nabvar/nabvar.component';

@NgModule({
  declarations: [
    FooterComponent,
    NabvarComponent,
  ],
  exports: [
    FooterComponent,
    NabvarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
  ]
})
export class SharedModule { }
