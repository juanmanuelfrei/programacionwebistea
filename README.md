# ProyectoPw

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Development server

Luego de clonar el proyecto se debera realizar un `npm install` para descargar las dependencias. Luego se podra correr la app utilizando `ng serve -o` (Por defecto levanta localhost:4200).

## Running unit tests

El proyecto no cuenta con Test adicionales.

## More

Se utilizo Bootstrap, Animate.css y Sweet Alert para la UI de las diferentes paginas.



